package az.ingress.controllers;

import az.ingress.dto.UserDto;
import az.ingress.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/user")
    public String getUser(Authentication authentication) {
        return "Hello User :" + authentication.getPrincipal();
    }

    @GetMapping("/admin")
    public String getAdmin() {
        return "Hello Admin";
    }

    @GetMapping("/public")
    public String getPublic() {
        return "Hello World";
    }

    @PostMapping
    public void createUser(@RequestBody @Valid UserDto userDto) throws Exception {
        userService.createUser(userDto);
    }

    @GetMapping
    public List<UserDto> getUsers() {
        return userService.listUsers();
    }


}
