package az.ingress.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
@Order(1)
public class LoggingFilter extends HttpFilter {

    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.info("=================================");
        log.info("Request received in filter {} {} {}", request, response, chain);
        log.info(request.getRequestURI());
        log.info(request.getMethod());
        log.info(request.getPathInfo());
        log.info(request.getLocalAddr());
        log.info(request.getAuthType());
        log.info(request.getRemoteUser());

        response.getHeaderNames().stream().forEach((h) -> System.out.println(h + "=" + response.getHeader(h)));

        chain.doFilter(request, response);

    }
}
