package az.ingress.repository;

import az.ingress.common.domain.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    @EntityGraph(value = "user_with_authorities")
    @Query("select u from User u")
    List<User> findAllIncludeAuthorities();
}
