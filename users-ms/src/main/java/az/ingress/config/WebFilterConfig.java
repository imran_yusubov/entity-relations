package az.ingress.config;

import az.ingress.filter.LoggingFilter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;

@Slf4j
@RequiredArgsConstructor
@Configuration
public class WebFilterConfig extends
        SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    @Override
    public void configure(HttpSecurity http) {
        log.info("Added request logging filter");
        http.addFilterBefore(new LoggingFilter(), SecurityContextPersistenceFilter.class);
    }
}
