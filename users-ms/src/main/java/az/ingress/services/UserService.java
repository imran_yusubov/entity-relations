package az.ingress.services;

import az.ingress.common.domain.User;
import az.ingress.dto.UserDto;
import az.ingress.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final ModelMapper mapper;

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public void createUser(UserDto userDto) {
        createUser2(userDto);
        ///
    }

    private void createUser2(UserDto userDto) {
        log.info("Creating user");
        User user = mapper.map(userDto, User.class);
        userRepository.save(user); //?
        //  throw new RuntimeException("Something went wrong");
    }

    @SneakyThrows
    //@Transactional
    public List<UserDto> listUsers() {
        List<UserDto> collect = userRepository.findAllIncludeAuthorities().stream().map((u) -> mapper.map(u, UserDto.class))
                .collect(Collectors.toList());
        return collect;
    }

    //LAZY EAGER -compile
}
