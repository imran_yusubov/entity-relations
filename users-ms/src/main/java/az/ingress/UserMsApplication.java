package az.ingress;

import az.ingress.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@SpringBootApplication
public class UserMsApplication implements CommandLineRunner {

    private final UserRepository userRepository;
  //  private final BCryptPasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(UserMsApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        log.info("Started");

//        User user = new User();
//        user.setPassword(passwordEncoder.encode("1234"));
//        user.setEmail("test@ingress.az");
//        userRepository.save(user);
    }
}
