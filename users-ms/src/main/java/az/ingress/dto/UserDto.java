package az.ingress.dto;

import az.ingress.common.domain.Authority;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

@Data
public class UserDto {

    private Long id;

    @NotEmpty(message = "email must be provided")
    @Email
    private String email;

    @NotEmpty
    private String password;

    @NotEmpty
    private String displayName;

    @NotEmpty
    private String organisation;

    private Set<Authority> authorities = new HashSet<>();
}
