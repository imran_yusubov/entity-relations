package az.ingress.common.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@Slf4j
public class JwtService {

    private Key key;

    public JwtService() {
        log.info("Initializing JWT secret key");
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode("U3BlY2lmaWNhdGlvbiAoUkZDIDc1MTgsIFNlY3Rpb24gMy4yKSBzdGF0ZXMgdGhhdCBrZXlzIHVzZWQgd2l0aCBITUFDLVNIQSBhbGdvcml0aG1zIE1VU1QgaGF2ZSBhIHNpemUg");
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public String issueToken() {
        log.trace("Issue JWT token to {} for {}");
        Duration gap = Duration.ofHours(2);
        String jwtToken = Jwts.builder()
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(gap)))
                .setSubject("imran")
                .addClaims(Map.of("role", Set.of("USER", "ADMIN", "SUPERUSER"), "authority", Set.of("USER", "ADMIN", "SUPERUSER"), "course", "MS5"))
                .signWith(key, SignatureAlgorithm.HS512)
                .compact();
        return jwtToken;
    }


    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public static void main(String[] args) {
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE2MjA0MDM1MjAsImV4cCI6MTYyMDQxMDcyMCwic3ViIjoiaW1yYW4iLCJyb2xlIjpbIlNVUEVSVVNFUiIsIlVTRVIiLCJBRE1JTiJdLCJhdXRob3JpdHkiOlsiU1VQRVJVU0VSIiwiVVNFUiIsIkFETUlOIl0sImNvdXJzZSI6Ik1TNSJ9.ZMH52kl-DDIAtAKExYgOv5DXFPCRdgtIXEnQSJiZiLW8FMM4QCDAa8_f2M7qqwIlkLYyMivS0VhcGL3ZcpEzcg";

        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode("U3BlY2lmaWNhdGlvbiAoUkZDIDc1MTgsIFNlY3Rpb24gMy4yKSBzdGF0ZXMgdGhhdCBrZXlzIHVzZWQgd2l0aCBITUFDLVNIQSBhbGdvcml0aG1zIE1VU1QgaGF2ZSBhIHNpemUg");
        Key key = Keys.hmacShaKeyFor(keyBytes);

        Claims body = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();

        System.out.println(body);

    }
}
