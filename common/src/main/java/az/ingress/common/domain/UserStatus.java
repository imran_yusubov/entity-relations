package az.ingress.common.domain;

public enum UserStatus {
    INACTIVE, ACTIVE, DELETED, BLOCKED
}
