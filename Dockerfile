FROM alpine:3.11.2
RUN apk add --no-cache openjdk8
COPY build/libs/multi-modle-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/multi-modle-0.0.1-SNAPSHOT.jar"]
