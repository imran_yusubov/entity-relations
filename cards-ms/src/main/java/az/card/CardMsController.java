package az.card;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardMsController {

    public static void main(String[] args) {
        SpringApplication.run(CardMsController.class, args);
    }
}
